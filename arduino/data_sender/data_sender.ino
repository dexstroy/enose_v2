#include "DHT.h"
#define DHTPIN 4     // Digital pin connected to the DHT sensor
#define DHTTYPE DHT22   // DHT 22  (AM2302), AM2321
DHT dht(DHTPIN, DHTTYPE);

// analog sensor pins
int S_1 = 34;
int S_2 = 35;
int S_3 = 25;
int S_4 = 26;

String message;
int character;

void setup() {
  Serial.begin(9600);
  dht.begin();
}

void loop() {
  if(Serial.available() > 0){
    character = Serial.read();
    if(character != 10){
      message.concat((char)character);
    } 
    else if(character == 10){
      //Serial.println(message + " receive");
      //Serial.println(analogRead(34));
      sendLine(message.toInt());
      message = "";
    }
  }


  /*
  // Wait a few seconds between measurements.
  delay(2000);

  // Reading temperature or humidity takes about 250 milliseconds!
  // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
  float h = dht.readHumidity();
  // Read temperature as Celsius (the default)
  float t = dht.readTemperature();
  // Read temperature as Fahrenheit (isFahrenheit = true)
  float f = dht.readTemperature(true);

  // Check if any reads failed and exit early (to try again).
  if (isnan(h) || isnan(t) || isnan(f)) {
    Serial.println(F("Failed to read from DHT sensor!"));
    return;
  }

  // Compute heat index in Fahrenheit (the default)
  float hif = dht.computeHeatIndex(f, h);
  // Compute heat index in Celsius (isFahreheit = false)
  float hic = dht.computeHeatIndex(t, h, false);

  Serial.print(F("Humidity: "));
  Serial.print(h);
  Serial.print(F("%  Temperature: "));
  Serial.print(t);
  Serial.print(F("°C "));
  Serial.print(f);
  Serial.print(F("°F  Heat index: "));
  Serial.print(hic);
  Serial.print(F("°C "));
  Serial.print(hif);
  Serial.println(F("°F"));
  */
}


void sendLine(int num){
  bool first = true;
  
  // S_1
  if((num & 32) > 0){
    if(!first){
      Serial.print(" ");
    }
    first = false;
    Serial.print(analogRead(S_1));
    
  }

  // S_2
  if((num & 16) > 0){
    if(!first){
      Serial.print(" ");
    }
    first = false;
    Serial.print(analogRead(S_2));
    
  }

  // S_3
  if((num & 8) > 0){
    if(!first){
      Serial.print(" ");
    }
    first = false;
    Serial.print(analogRead(S_3));
  }

  // S_4
  if((num & 4) > 0){
    if(!first){
      Serial.print(" ");
      
    }
    first = false;
    Serial.print(analogRead(S_4));
  }

  // DHT_temp
  if((num & 2) > 0){
    if(!first){
      Serial.print(" ");
    }
    first = false;
    float temp = dht.readTemperature();
    //float temp = 10.2;
    if(isnan(temp)){
      temp = 0.0;  
    }
    Serial.print((int)temp);
  }

  // DHT_hum
  if((num & 1) > 0){
 
    if(!first){
      Serial.print(" ");
    }
    first = false;
    float hum = dht.readHumidity();
    //float hum = 22.0;
    if(isnan(hum)){
      hum = 0.0;  
    }
    Serial.print((int)hum);
  }
  Serial.println("");    
}
