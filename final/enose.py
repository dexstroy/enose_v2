from PyQt5.QtCore import pyqtSignal, Qt
from PyQt5.QtGui import QPixmap, QFont, QFontDatabase
from  PyQt5.QtWidgets import *
from PyQt5 import QtCore
import time

import os.path
import sys
import glob
import serial
from tabulate import tabulate
from time import localtime, strftime

import json

# outside function shared between all classes
def fill_right(text, width):
    spaces = width - len(text)
    if spaces < 0:
        return text[0:width]

    return text + (" " * spaces)


def fill_left(text, width):
    spaces = width - len(text)
    if spaces < 0:
        return text[0:width]

    return (" " * spaces) + text


# custom QLineEdit with implemented mouse click listener
class ClickableLineEdit(QLineEdit):
    clicked = pyqtSignal() # signal when the text entry is left clicked

    def mousePressEvent(self, event):
        if event.button() == Qt.LeftButton: self.clicked.emit()
        else: super().mousePressEvent(event)


# search all available serial ports
def serial_ports():
    """ Lists serial port names

        :raises EnvironmentError:
            On unsupported or unknown platforms
        :returns:
            A list of the serial ports available on the system
    """
    if sys.platform.startswith('win'):
        ports = ['COM%s' % (i + 1) for i in range(256)]
    elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
        # this excludes your current terminal "/dev/tty"
        ports = glob.glob('/dev/tty[A-Za-z]*')
    elif sys.platform.startswith('darwin'):
        ports = glob.glob('/dev/tty.*')
    else:
        raise EnvironmentError('Unsupported platform')

    result = []
    for port in ports:
        try:
            s = serial.Serial(port)
            s.close()
            result.append(port)
        except (OSError, serial.SerialException):
            pass
    return result


class ComRefreshThread(QtCore.QThread):
    comPorts = QtCore.pyqtSignal(list)

    def __init__(self, *args, **kwargs):
        QtCore.QThread.__init__(self, *args, **kwargs)
        self.running = True

    def run(self):
        while self.running:
            self.comPorts.emit(serial_ports())
            time.sleep(1)


class TestTimeTimer(QtCore.QThread):
    test_time_alarm = QtCore.pyqtSignal()

    def __init__(self, *args, **kwargs):
        QtCore.QThread.__init__(self, *args, **kwargs)
        self.test_time_m = 1
        self.running = True

    def stop_thread(self):
        self.running = False

    def run(self):
        start_time = time.time() / 60

        while self.running:
            if ((time.time() / 60) - start_time) >= self.test_time_m:

                self.test_time_alarm.emit()
                self.running = False
                break
            time.sleep(0.1)



class DataCaptureThread(QtCore.QThread):
    dataChanged = QtCore.pyqtSignal(str)

    def __init__(self, *args, **kwargs):
        QtCore.QThread.__init__(self, *args, **kwargs)
        self._running = True
        self.interval_s = 5
        self.ser = serial.Serial(timeout=1)
        self.ser.baudrate = 9600
        self.connected = False

    def set_interval_s(self, interval):
        self.interval_s = interval

    def terminate(self):
        self._running = False

    def is_connected(self):
        return self.connected

    def connect(self, port):
        self.ser.port = port

        try:
            self.ser.open()
            self.connected = True
        except:
            return False
        print("Connected to " + port)
        return True

    def run(self):
        start_time = time.time()
        first_time = True

        while self._running:
            difference = time.time() - start_time
            if difference >= self.interval_s or first_time:
                start_time = time.time() - (difference - self.interval_s)
                succ_reading = False
                reading = ""

                while not succ_reading:
                    self.ser.write(b"63\n")
                    reading = self.ser.readline()
                    if reading.decode() != "":
                        succ_reading = True
                    else:
                        print("Failed to read____________________________")

                reading = reading.decode().replace("\n", "")
                self.dataChanged.emit(reading)

                first_time = False

            time.sleep(0.001)

        print("Thread terminated")


class Window(QWidget):
    def __init__(self):
        self.data_writer = DataWriter()

        QWidget.__init__(self)
        self.setWindowTitle("Recorder")
        self.resize(600, 300)
        layout = QGridLayout()
        self.setLayout(layout)

        self.data_capture_thread = None

        self.test_time_timer = None

        self.updating_ports = True

        # Top panel
        self.top_panel = QGridLayout()
        self.SS_setup_button = QPushButton("SS_setup", self)
        self.measurements_button = QPushButton("Measurements", self)
        self.measurements_button.setEnabled(False)
        self.exit_button = QPushButton("EXIT", self)

        self.exit_button.clicked.connect(self.exit_button_event)

        self.top_panel.addWidget(self.SS_setup_button, 0, 0)
        self.top_panel.addWidget(self.measurements_button, 0, 1)
        self.top_panel.addWidget(self.exit_button, 0, 2)

        # COM connection
        self.com_connection_layout = QGridLayout()

        self.com_combo = QComboBox()

        self.ports = serial_ports()
        self.com_combo.addItems(self.ports)

        self.com_thread = ComRefreshThread(self)
        self.com_thread.comPorts.connect(self.updateComPorts)
        self.com_thread.start()

        self.connect_button = QPushButton("connect", self)
        self.connect_button.clicked.connect(self.button_com_connect_event)


        self.status_message = QLabel(self)

        self.status_light_pictures = [
            QPixmap("./semafor/white.png"),
            QPixmap("./semafor/grey.png"),
            QPixmap("./semafor/yellow.png"),
            QPixmap("./semafor/green.png"),
            QPixmap("./semafor/red.png")
        ]

        self.status_light_message = [
            "",
            "connected",
            "run",
            "test",
            "error"
        ]

        self.status_light = QLabel(self)
        self.set_status_light(3)

        self.com_connection_layout.addWidget(self.com_combo, 0, 0)
        self.com_connection_layout.addWidget(self.connect_button, 0, 1)

        self.status_light_layout = QGridLayout()
        self.status_light_layout.setAlignment(Qt.AlignRight)
        self.status_light_layout.addWidget(self.status_message, 0, 0)
        self.status_light_layout.addWidget(self.status_light, 0, 1)

        status_light_widget = QWidget()
        status_light_widget.setLayout(self.status_light_layout)

        self.com_connection_layout.addWidget(status_light_widget, 0, 2)


        # capture parameter
        self.capture_parameters = QGridLayout()

        self.results_folder_edit = ClickableLineEdit('')
        self.file_name_run_edit = QLineEdit(self)
        self.file_name_test_edit = QLineEdit(self)
        self.test_interval_edit = QLineEdit(self)
        self.test_time_edit = QLineEdit(self)
        self.comment_edit = QLineEdit(self)

        self.results_folder_label = QLabel(self)
        self.file_name_run_label = QLabel(self)
        self.file_name_test_label = QLabel(self)
        self.test_interval_label = QLabel(self)
        self.test_time_label = QLabel(self)
        self.comment_label = QLabel(self)

        self.results_folder_label.setText("Results_folder:")
        self.file_name_run_label.setText("File_name_run:")
        self.file_name_test_label.setText("File_name_test:")
        self.test_interval_label.setText("Test_interval [s]:")
        self.test_time_label.setText("Test_time [min]:")
        self.comment_label.setText("Comment:")

        self.capture_parameters.addWidget(self.results_folder_label, 0, 0)
        self.capture_parameters.addWidget(self.file_name_run_label, 1, 0)
        self.capture_parameters.addWidget(self.file_name_test_label, 2, 0)
        self.capture_parameters.addWidget(self.test_interval_label, 3, 0)
        self.capture_parameters.addWidget(self.test_time_label, 4, 0)
        self.capture_parameters.addWidget(self.comment_label, 5, 0)

        self.capture_parameters.addWidget(self.results_folder_edit, 0, 1)
        self.results_folder_edit.setReadOnly(True)
        self.results_folder_edit.clicked.connect(self.get_results_folder)

        self.capture_parameters.addWidget(self.file_name_run_edit, 1, 1)
        self.capture_parameters.addWidget(self.file_name_test_edit, 2, 1)
        self.capture_parameters.addWidget(self.test_interval_edit, 3, 1)
        self.capture_parameters.addWidget(self.test_time_edit, 4, 1)
        self.capture_parameters.addWidget(self.comment_edit, 5, 1)

        # sensor parameters
        self.sensors_parameters = QGridLayout()

        self.S_1_label = QLabel(self)
        self.S_2_label = QLabel(self)
        self.S_3_label = QLabel(self)
        self.S_4_label = QLabel(self)

        self.S_1_label.setText("CH1:")
        self.S_1_label.setFixedWidth(20)
        self.S_2_label.setText("CH2:")
        self.S_3_label.setText("CH3:")
        self.S_4_label.setText("CH4:")

        self.sensors = ["None", "MQ-3", "MQ-135", "MQ-136", "MQ-137", "MQ-138"]

        self.S_1_combo = QComboBox()
        self.S_2_combo = QComboBox()
        self.S_3_combo = QComboBox()
        self.S_4_combo = QComboBox()

        self.S_1_combo.addItems(self.sensors)
        self.S_2_combo.addItems(self.sensors)
        self.S_3_combo.addItems(self.sensors)
        self.S_4_combo.addItems(self.sensors)

        self.F_1_label = QLabel(self)
        self.F_2_label = QLabel(self)
        self.F_3_label = QLabel(self)
        self.F_4_label = QLabel(self)

        self.F_1_label.setText("f1")
        self.F_1_label.setFixedWidth(15)
        self.F_2_label.setText("f2")
        self.F_3_label.setText("f3")
        self.F_4_label.setText("f4")

        self.F_1_edit = QLineEdit(self)
        self.F_2_edit = QLineEdit(self)
        self.F_3_edit = QLineEdit(self)
        self.F_4_edit = QLineEdit(self)

        self.F_1_edit.setFixedWidth(50)
        self.F_2_edit.setFixedWidth(50)
        self.F_3_edit.setFixedWidth(50)
        self.F_4_edit.setFixedWidth(50)

        self.status_light_CH1 = QLabel(self)
        self.status_light_CH1.setPixmap(QPixmap("./semafor/green.png"))
        self.status_light_CH2 = QLabel(self)
        self.status_light_CH2.setPixmap(QPixmap("./semafor/green.png"))
        self.status_light_CH3 = QLabel(self)
        self.status_light_CH3.setPixmap(QPixmap("./semafor/green.png"))
        self.status_light_CH4 = QLabel(self)
        self.status_light_CH4.setPixmap(QPixmap("./semafor/green.png"))

        #self.S_1_combo.setEnabled(False)

        self.sensors_parameters.addWidget(self.S_1_label, 0, 0)
        self.sensors_parameters.addWidget(self.S_2_label, 1, 0)
        self.sensors_parameters.addWidget(self.S_3_label, 2, 0)
        self.sensors_parameters.addWidget(self.S_4_label, 3, 0)

        self.sensors_parameters.addWidget(self.S_1_combo, 0, 1)
        self.sensors_parameters.addWidget(self.S_2_combo, 1, 1)
        self.sensors_parameters.addWidget(self.S_3_combo, 2, 1)
        self.sensors_parameters.addWidget(self.S_4_combo, 3, 1)

        self.sensors_parameters.addWidget(self.F_1_label, 0, 2)
        self.sensors_parameters.addWidget(self.F_2_label, 1, 2)
        self.sensors_parameters.addWidget(self.F_3_label, 2, 2)
        self.sensors_parameters.addWidget(self.F_4_label, 3, 2)

        self.sensors_parameters.addWidget(self.F_1_edit, 0, 3)
        self.sensors_parameters.addWidget(self.F_2_edit, 1, 3)
        self.sensors_parameters.addWidget(self.F_3_edit, 2, 3)
        self.sensors_parameters.addWidget(self.F_4_edit, 3, 3)

        self.sensors_parameters.addWidget(self.status_light_CH1, 0, 4)
        self.sensors_parameters.addWidget(self.status_light_CH2, 1, 4)
        self.sensors_parameters.addWidget(self.status_light_CH3, 2, 4)
        self.sensors_parameters.addWidget(self.status_light_CH4, 3, 4)

        # sensor data display
        self.sensors_data_display = QPlainTextEdit(self)
        self.font = QFont()

        self.font = QFont("Consolas", 10)
        self.font.setFixedPitch(True)
        self.sensors_data_display.setFont(self.font)
        self.sensors_data_display.setReadOnly(True)
        self.sensors_data_display.setMinimumHeight(125)


        self.recording_data_display = QPlainTextEdit(self)
        self.font = QFont()

        self.font = QFont("Consolas", 10)
        self.font.setFixedPitch(True)
        self.recording_data_display.setFont(self.font)
        self.recording_data_display.setReadOnly(True)
        self.recording_data_display.setMinimumHeight(125)


        # control buttons
        self.control_buttons = QGridLayout()

        self.SS_run_start = QPushButton("SS_run_start", self)
        self.SS_test_start = QPushButton("SS_test_start", self)
        self.SS_test_stop = QPushButton("SS_test_stop", self)
        self.SS_run_stop = QPushButton("SS_run_stop", self)

        self.SS_run_start.clicked.connect(self.button_SS_run_start_event)
        self.SS_test_start.clicked.connect(self.button_SS_test_start_event)
        self.SS_test_stop.clicked.connect(self.button_SS_test_stop_event)
        self.SS_run_stop.clicked.connect(self.button_SS_run_stop_event)

        #self.SS_run_stop.setEnabled(False)

        self.control_buttons.addWidget(self.SS_run_start, 0, 0)
        self.control_buttons.addWidget(self.SS_test_start, 0, 1)
        self.control_buttons.addWidget(self.SS_test_stop, 0, 2)
        self.control_buttons.addWidget(self.SS_run_stop, 0, 3)

        # widgets grouping

        top_panel_widget = QWidget()
        top_panel_widget.setLayout(self.top_panel)

        com_connection_widget = QGroupBox("Connection parameters")
        com_connection_widget.setLayout(self.com_connection_layout)

        capture_parameters_widget = QGroupBox("Capture parameters")
        capture_parameters_widget.setLayout(self.capture_parameters)

        sensors_parameters_widget = QGroupBox("Selected sensors")
        sensors_parameters_widget.setLayout(self.sensors_parameters)

        control_buttons_widget = QWidget()
        control_buttons_widget.setLayout(self.control_buttons)

        layout.addWidget(top_panel_widget, 0, 0, 1, 2)

        layout.addWidget(com_connection_widget, 1, 0, 1, 2)

        layout.addWidget(capture_parameters_widget, 2, 0)

        layout.addWidget(sensors_parameters_widget, 2, 1)

        layout.addWidget(self.sensors_data_display, 3, 0, 1, 2)

        layout.addWidget(self.recording_data_display, 4, 0, 1, 2)

        layout.addWidget(control_buttons_widget, 5, 0, 1, 2)

        self.limit_record_buttons()

        self.update_measurement_text()

        self.set_status_light(0)



        #self.fill_forms()
        self.load_data_writer_input_parameters()

    # autofill based on saved form, from previous use
    def fill_saved_form(self):
        pass

    # auto fill forms (only for debugging and testing)
    def fill_forms(self):
        self.file_name_run_edit.setText("run")
        self.file_name_test_edit.setText("test")
        self.test_interval_edit.setText("1")
        self.test_time_edit.setText("5")
        self.comment_edit.setText("To je komentar")

        self.S_1_combo.setCurrentText(self.sensors[1])
        self.S_2_combo.setCurrentText(self.sensors[2])
        self.S_3_combo.setCurrentText(self.sensors[3])
        self.S_4_combo.setCurrentText(self.sensors[5])

        print("fill forms called")

    def data_capture_event(self, text):

        print("Thread called: " + text)


        if self.data_writer.run_capture:
            self.data_writer.write_run_sample(text)
            self.update_measurement_log()

        if self.data_writer.test_capture:
            self.data_writer.write_test_sample(text)

    def updateComPorts(self, ports):
        if self.ports != ports and self.updating_ports:
            self.ports = ports
            self.com_combo.clear()
            self.com_combo.addItems(ports)

    def test_time_stop_timer_event(self):
        self.set_status_light(2)

    def button_com_connect_event(self):

        self.updating_ports = False
        self.com_combo.setEnabled(False)
        self.data_capture_thread = DataCaptureThread(self)
        self.data_capture_thread.dataChanged.connect(self.data_capture_event)
        is_connected = self.data_capture_thread.connect(self.com_combo.currentText())
        if is_connected:
            self.set_status_light(1)
            self.connect_button.setEnabled(False)
            self.limit_record_buttons()
        else:
            self.set_status_light(4)

    def button_SS_run_start_event(self):
        self.update_data_writer_input_parameters()

        if not self.data_writer.check_if_ok_to_override():
            return

        if self.data_writer.validate_data():
            self.data_writer.set_run_capture(True)
            self.limit_record_buttons()
            self.data_capture_thread.set_interval_s(self.data_writer.test_interval_s)
            self.data_capture_thread.start()
            self.update_measurement_text()
            self.set_status_light(2)


    def button_SS_test_start_event(self):
        print("button_SS_test_start_event")
        self.data_writer.set_test_capture(True)
        self.limit_record_buttons()
        self.update_measurement_text()
        self.set_status_light(3)

        self.test_time_timer = TestTimeTimer()
        self.test_time_timer.test_time_m = self.data_writer.test_time_min
        self.test_time_timer.test_time_alarm.connect(self.test_time_stop_timer_event)
        self.test_time_timer.start()

    def button_SS_test_stop_event(self):
        print("button_SS_test_stop_event")
        self.data_writer.set_test_capture(False)
        self.limit_record_buttons()
        self.data_writer.save_test_capture()
        self.update_measurement_text()
        self.test_time_timer.stop_thread()
        self.set_status_light(2)

    def button_SS_run_stop_event(self):
        print("button_SS_run_stop_event")
        self.data_capture_thread.terminate()
        self.data_writer.set_run_capture(False)
        self.data_writer.set_test_capture(False)
        self.limit_record_buttons()
        self.data_writer.save_test_capture()
        self.data_writer.save_run_capture()
        self.update_measurement_text(min_max_avg=True)
        self.set_status_light(1)

    def limit_record_buttons(self):
        self.SS_run_start.setEnabled((not self.data_writer.run_capture) and (self.data_capture_thread is not None) and
                                     self.data_capture_thread.is_connected())
        self.SS_run_stop.setEnabled(self.data_writer.run_capture)

        self.SS_test_start.setEnabled((not self.data_writer.test_capture) and self.data_writer.run_capture)
        self.SS_test_stop.setEnabled(self.data_writer.test_capture)

    '''
    light_code:
        0: White (no text)
        1: Grey (connected)
        2: Yellow (run)
        3: Green (test)
        4: Red (error)
    '''
    def set_status_light(self, light_code):
        if light_code < len(self.status_light_pictures):
            self.status_light.setPixmap(self.status_light_pictures[light_code])
            self.status_message.setText(self.status_light_message[light_code])

    def get_results_folder(self):
        folderpath = QFileDialog.getExistingDirectory(self, 'Select Folder')
        self.results_folder_edit.setText(folderpath)

    def update_measurement_text(self, min_max_avg=False):
        final_string = ""
        spacing = [20, 27, 20, 9]

        final_string += fill_right("Date:", spacing[0]) + fill_right(self.data_writer.date, spacing[1]) + fill_right("Test start:", spacing[2]) + fill_left(self.data_writer.test_start_time, spacing[3]) + "\n"
        final_string += fill_right("Run_start:", spacing[0]) + fill_right(self.data_writer.run_start_time, spacing[1]) + fill_right("Test stop:", spacing[2]) + fill_left(self.data_writer.test_stop_time, spacing[3]) + "\n"
        final_string += fill_right("Run_stop:", spacing[0]) + fill_right(self.data_writer.run_stop_time, spacing[1]) + "Room_T [°C](min - max - avg):" + "\n"
        final_string += fill_right("Sensors:", spacing[0]) + fill_right(self.data_writer.generate_selected_sensors(), spacing[1]) + (" " * 12) + ("" if not min_max_avg else self.data_writer.get_min_max_avg_temperature().replace(",", " - ")) + "\n"
        final_string += fill_right("Test_time [min]:", spacing[0]) + fill_right(str(self.data_writer.get_test_time_min()), spacing[1]) + "Room_RH [%](min - max - avg):" + "\n"
        final_string += fill_right("Test_interval [s]:", spacing[0]) + fill_right(str(self.data_writer.get_test_interval_s()), spacing[1]) + (" " * 12) + ("" if not min_max_avg else self.data_writer.get_min_max_avg_humidity().replace(",", " - ")) + "\n"

        self.sensors_data_display.setPlainText(final_string)

    def update_measurement_log(self):
        self.recording_data_display.setPlainText(tabulate(self.data_writer.run_samples[-5:], headers=self.data_writer.get_table_header()))

    def update_data_writer_input_parameters(self):
        if self.data_writer is None:
            return

        # capture parameters
        self.data_writer.results_folder = self.results_folder_edit.text()
        self.data_writer.file_name_run = self.file_name_run_edit.text()
        self.data_writer.file_name_test = self.file_name_test_edit.text()
        if self.test_interval_edit.text().isdigit():
            self.data_writer.test_interval_s = int(self.test_interval_edit.text())
        if self.test_time_edit.text().isdigit():
            self.data_writer.test_time_min = int(self.test_time_edit.text())
        self.data_writer.comment = self.comment_edit.text()

        # selected sensors
        self.data_writer.selected_sensors = [self.S_1_combo.currentText(),
                                             self.S_2_combo.currentText(),
                                             self.S_3_combo.currentText(),
                                             self.S_4_combo.currentText()]

    # parameters defined in datawriter are displayed in forms
    def load_data_writer_input_parameters(self):
        self.data_writer.load_form_data()
        self.results_folder_edit.setText(self.data_writer.results_folder)
        self.file_name_run_edit.setText(self.data_writer.file_name_run)
        self.file_name_test_edit.setText(self.data_writer.file_name_test)

        if self.data_writer.test_interval_s is not None:
            self.test_interval_edit.setText(str(self.data_writer.test_interval_s))

        if self.data_writer.test_time_min is not None:
            self.test_time_edit.setText(str(self.data_writer.test_time_min))

        self.comment_edit.setText(self.data_writer.comment)
        if len(self.data_writer.selected_sensors) >= 4:
            self.S_1_combo.setCurrentText(self.data_writer.selected_sensors[0])
            self.S_2_combo.setCurrentText(self.data_writer.selected_sensors[1])
            self.S_3_combo.setCurrentText(self.data_writer.selected_sensors[2])
            self.S_4_combo.setCurrentText(self.data_writer.selected_sensors[3])

    def closeEvent(self, QCloseEvent):
        self.update_data_writer_input_parameters()
        self.data_writer.save_form_data()

    def exit_button_event(self):
        self.close()


# class for writing recording data
class DataWriter:

    def __init__(self):
        self.run_capture = False
        self.test_capture = False

        # input parameters
        self.results_folder = ""
        self.file_name_run = ""
        self.file_name_test = ""
        self.test_interval_s = None
        self.test_time_min = None
        self.comment = ""
        self.selected_sensors = []

        # file writers
        self.file_run = None
        self.file_test = None

        # number sample counter
        self.run_sample_counter = 1
        self.test_sample_counter = 1

        # header parameters
        self.date = ""
        self.run_start_time = ""
        self.run_stop_time = ""
        self.test_start_time = ""
        self.test_stop_time = ""

        # Temperature
        self.min_temperature = 100
        self.max_temperature = -100
        self.temperature_sum = 0

        # Humidity
        self.min_humidity = 100
        self.max_humidity = 0
        self.humidity_sum = 0

        # run samples and header for displaying
        self.run_samples = []
        self.check_if_form_data_exists()

    # in case if file does not exists it creates a new one
    def check_if_form_data_exists(self):
        if os.path.isfile("./formData.json"):
            return True
        self.save_form_data()
        return False

    def save_form_data(self):
        # Data to be written
        dictionary = {
            "results_folder": self.results_folder,
            "file_name_run": self.file_name_run,
            "file_name_test": self.file_name_test,
            "test_interval_s": self.test_interval_s,
            "test_time_min": self.test_time_min,
            "comment": self.comment,
            "selected_sensors": self.selected_sensors
        }

        # Serializing json
        json_object = json.dumps(dictionary, indent=4)

        # Writing to formData.json
        with open("formData.json", "w") as outfile:
            outfile.write(json_object)

    def load_form_data(self):
        if not os.path.isfile("./formData.json"):
            self.save_form_data()

        with open('formData.json', 'r') as openfile:
            # Reading from json file
            dictionary = json.load(openfile)
            self.results_folder = dictionary["results_folder"]
            self.file_name_run = dictionary["file_name_run"]
            self.file_name_test = dictionary["file_name_test"]
            self.test_interval_s = dictionary["test_interval_s"]
            self.test_time_min = dictionary["test_time_min"]
            self.comment = dictionary["comment"]
            self.selected_sensors = dictionary["selected_sensors"]

    def check_if_ok_to_override(self):
        run_exists = os.path.isfile(self.results_folder + "/" + self.add_csv_extension(self.file_name_run))
        test_exists = os.path.isfile(self.results_folder + "/" + self.add_csv_extension(self.file_name_test))

        if run_exists or test_exists:
            message = "File_name_run and File_name_test already exists, do you want to override them?" if (run_exists and test_exists) \
                else ("File_name_run " if run_exists else "File_name_test ") + "already exists, do you want to override it?"
            msg_box = QMessageBox()
            msg_box.setIcon(QMessageBox.Information)
            msg_box.setText(message)
            msg_box.setWindowTitle("Files with same name detected")
            msg_box.setStandardButtons(QMessageBox.Yes | QMessageBox.No)

            return_value = msg_box.exec()
            if return_value == QMessageBox.No:
                return False
        return True

    def get_table_header(self):
        return ["i"] + self.generate_selected_sensors().split(",") + ["T", "RH"]

    def get_min_max_avg_temperature(self):
        return str(round(float(self.min_temperature), 1)) + "," + \
               str(round(float(self.max_temperature), 1)) + "," + \
               str(round(float(self.temperature_sum / self.run_sample_counter), 1))

    def get_min_max_avg_humidity(self):
        return str(round(float(self.min_humidity), 1)) + "," + \
               str(round(float(self.max_humidity), 1)) + "," + \
               str(round(float(self.humidity_sum / self.run_sample_counter), 1))

    def get_average_humidity(self):
        return self.humidity_sum / self.run_sample_counter

    def set_run_capture(self, state):
        if self.run_capture == state:
            return
        self.run_capture = state
        if state:
            self.date = strftime("%d.%m.%Y", localtime())
            self.run_start_time = strftime("%H:%M:%S", localtime())
        else:
            self.run_stop_time = strftime("%H:%M:%S", localtime())

    def set_test_capture(self, state):
        if self.test_capture == state:
            return
        self.test_capture = state
        if state:
            self.test_start_time = strftime("%H:%M:%S", localtime())
        else:
            self.test_stop_time = strftime("%H:%M:%S", localtime())

    # check if all data is suitable for starting recording
    def validate_data(self):
        if self.results_folder == "" or self.file_name_run == "" or self.file_name_test == "" or self.test_interval_s is None or self.test_time_min is None or not self.at_least_one_sensor_selected():
            return False
        return True

    def add_csv_extension(self, filename):
        return filename + ("" if (".csv" in filename) else ".csv")

    def write_run_sample(self, sample):
        if self.file_name_run == "" or self.results_folder == "":
            return False
        if self.file_run is None:
            self.file_run = open(self.results_folder + "/" + self.add_csv_extension(self.file_name_run), "w")

        # [CH_1, CH_2, CH_3, CH_4, DHT_temp, DHT_hum]
        sample = sample.split(" ")

        line = str(self.run_sample_counter)
        sample_line = [self.run_sample_counter]
        for i in range(len(self.selected_sensors)):
            if self.selected_sensors[i] != "None":
                line += "," + sample[i]
                sample_line.append(sample[i])
        self.run_samples.append(sample_line + sample[-2:])
        self.file_run.write(line + "\n")
        temperature = int(sample[4])
        humidity = int(sample[5])

        if temperature < self.min_temperature:
            self.min_temperature = temperature
        if temperature > self.max_temperature:
            self.max_temperature = temperature
        self.temperature_sum += temperature

        if humidity < self.min_humidity:
            self.min_humidity = humidity
        if humidity > self.max_humidity:
            self.max_humidity = humidity
        self.humidity_sum += humidity

        self.run_sample_counter += 1
        return True

    def write_test_sample(self, sample):
        if self.file_name_test == "" or self.results_folder == "":
            return False
        if self.file_test is None:
            self.file_test = open(self.results_folder + "/" + self.add_csv_extension(self.file_name_test), "w")

        # [CH_1, CH_2, CH_3, CH_4, DHT_temp, DHT_hum]
        sample = sample.split(" ")
        line = str(self.test_sample_counter)
        for i in range(len(self.selected_sensors)):
            if self.selected_sensors[i] != "None":
                line += "," + sample[i]
        self.file_test.write(line + "\n")
        self.test_sample_counter += 1
        return True

    def save_run_capture(self):
        if self.file_run is None:
            return False
        self.file_run.close()
        self.file_run = None

        # insert header at begining of the file

        with open(self.results_folder + "/" + self.add_csv_extension(self.file_name_run), 'r+') as f:
            content = f.read()
            f.seek(0, 0)
            f.write(self.generate_run_header() + content)
        return True

    def save_test_capture(self):
        if self.file_test is None:
            return False
        self.file_test.close()
        self.file_test = None

        # insert header at begining of the file

        with open(self.results_folder + "/" + self.add_csv_extension(self.file_name_test), 'r+') as f:
            content = f.read()
            f.seek(0, 0)
            f.write(self.generate_test_header() + content)
        return True

    def generate_run_header(self):
        return "Date," + self.date + "\n" + \
                     "Comment," + self.comment + "\n" + \
                     "Run_start," + self.run_start_time + "\n" + \
                     "Run_stop," + self.run_stop_time + "\n" + \
                     "Sensors," + self.generate_selected_sensors() + "\n" + \
                     "Test_time [min]," + str(self.test_time_min) + "\n" + \
                     "Test_interval [s]," + str(self.test_interval_s) + "\n" + \
                     "Test_start," + self.test_start_time + "\n" + \
                     "Test_stop," + self.test_stop_time + "\n" + \
                     "Room_T [C] (min - max - avg)," + self.get_min_max_avg_temperature() + "\n" + \
                     "Room_RH [%] (min - max - avg)," + self.get_min_max_avg_humidity() + "\n" + \
                     "Measurement_i," + self.generate_selected_sensors() + "\n"

    def generate_test_header(self):
        return "Date," + self.date + "\n" + \
                     "Comment," + self.comment + "\n" + \
                     "Test_start," + self.test_start_time + "\n" + \
                     "Test_stop," + self.test_stop_time + "\n" + \
                     "Sensors," + self.generate_selected_sensors() + "\n" + \
                     "Test_time [min]," + str(self.test_time_min) + "\n" + \
                     "Test_interval [s]," + str(self.test_interval_s) + "\n" + \
                     "Room_T [C] (min - max - avg)," + self.get_min_max_avg_temperature() + "\n" + \
                     "Room_RH [%] (min - max - avg)," + self.get_min_max_avg_humidity() + "\n" + \
                     "Measurement_i," + self.generate_selected_sensors() + "\n"

    def generate_selected_sensors(self):
        output = ""
        for sensor in self.selected_sensors:
            if sensor != "None":
                if len(output) == 0:
                    output += sensor
                else:
                    output += "," + sensor
        return output

    def get_test_time_min(self):
        if self.test_time_min is None:
            return ""
        return self.test_time_min

    def get_test_interval_s(self):
        if self.test_interval_s is None:
            return ""
        return self.test_interval_s

    def at_least_one_sensor_selected(self):
        for sensor in self.selected_sensors:
            if sensor != "None":
                return True
        return False


app = QApplication(sys.argv)

screen = Window()
screen.show()

sys.exit(app.exec_())