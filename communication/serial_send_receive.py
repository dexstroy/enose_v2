from PyQt5.QtWidgets import *
from PyQt5.QtCore import QTimer
import sys
import serial
import threading

class SerialSender(QWidget):
    def __init__(self):
        QWidget.__init__(self)
        # serial setup
        self.ser = serial.Serial()
        self.ser.baudrate = 9600
        self.ser.port = 'COM6'
        self.ser.open()

        # GUI variables
        self.setWindowTitle("Hello")
        self.resize(400, 300)
        layout = QGridLayout()
        self.setLayout(layout)

        self.textbox = QLineEdit(self)
        layout.addWidget(self.textbox, 0, 0)

        self.send_button = QPushButton("Send", self)
        layout.addWidget(self.send_button, 0, 1)
        self.send_button.clicked.connect(self.send_button_event)

        self.event_box = QPlainTextEdit(self)
        self.event_box.setReadOnly(True)
        self.event_box.setMinimumHeight(250)
        layout.addWidget(self.event_box, 1, 0)

        self.thread = threading.Thread(target=self.check_for_receive)
        self.thread.start()


    def send_button_event(self):
        if len(self.textbox.text()) == 0:
            return

        self.write_send_event(self.textbox.text())
        self.ser.write(str.encode(self.textbox.text() + "\n"))
        # self.write_receive_event(self.ser.readline(100))
        self.textbox.setText("")

    def write_send_event(self, string):
        self.event_box.setPlainText(self.event_box.toPlainText() + "[Send]: " + string + "\n")

    def write_receive_event(self, string):
        #self.event_box.setPlainText(self.event_box.toPlainText() + "[Recv]: " + string + "\n")
        self.event_box.setPlainText("test")

    def check_for_receive(self):
        while True:
            print("Receive cycle")
            reading = self.ser.readline()
            self.ser.timeout
            reading = reading.decode().replace("\n", "")
            self.write_receive_event(reading)
            print(reading)


app = QApplication(sys.argv)

screen = SerialSender()
screen.show()



sys.exit(app.exec_())
