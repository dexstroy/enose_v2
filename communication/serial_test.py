import serial

'''
with serial.Serial('COM12', 9600, timeout=1) as ser:
    ser.write(b'hello')
    line = ser.readline()
    print(line)
'''

import serial.tools.list_ports

ports = serial.tools.list_ports.comports()

print([port.name for port in ports])