# System for Measuring Fruit Ripening Stages

## About
The system for measuring fruit ripening stages using gas sensors.

We place fruit in a glass jar and measure gas concentration for 30 minutes. Based on those measurements, we can predict fruit ripening stage.

![Local Image](images/marelice_measure.jpg)

## Prototype
The prototype was implemented on a protoboard.

![Local Image](images/WiredSensors.jpg)

Software for capturing data was very simple and was controlled from the terminal.

## Final product
The final product has a custom 3D printed holder that was designed in [FreeCAD](https://www.freecad.org/). Electronic components are connected with a custom-made PCB designed in [KiCad](https://www.kicad.org/) and carved on a CNC machine. There is a special connector for connecting up to 5 gas sensors.

![Local Image](images/senzorskiSistem.jpg)

Software for capturing data evolved from a terminal-based to full GUI support and has many features for recording data.

![Local Image](images/enose_program.png)
